#概述
一个基于ThinkPhP5的CMS系统；
ThinkBlog是我在学习新版ThinkPhP5的时候练习的一个作品。
目前只有一个后端管理功能，我会慢慢完善的。
希望能够和大家一起学习交流、探讨。


**目前已经有的功能：**

* 文章管理；
* 分类管理；
* 链接管理；
* 配置项管理；
* 用户信息修改；
* 密码修改；

**安装文档**：http://www.kancloud.cn/thinkblog/thinkblog/221789
**QQ交流群**：455818667
**在线文档**：http://www.kancloud.cn/thinkblog/thinkblog